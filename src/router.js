import { createRouter, createWebHistory } from 'vue-router';

import HomeRoom from './pages/homeroom/HomeRoom.vue';
import GeneralRoom from './pages/gameroom/GeneralRoom.vue';
import GameRoom from './pages/gameroom/GameRoom.vue';
import AboutApp from './pages/about/AboutApp.vue'
import NotFound from './pages/NotFound.vue';


const router = createRouter({
    history: createWebHistory(),
    routes: [
        { path: '/', redirect: '/homeroom' },
        { path: '/homeroom', component: HomeRoom },
        { path: '/generalroom/:roomName', component: GeneralRoom, props: true,
            children: [
                { path: 'gameroom', component: GameRoom }
            ]},
        { path: '/about', component: AboutApp },   
        { path: '/:notFound(.*)*', component: NotFound }
    ]
});

export default router;