import axios from 'axios';
import router from '../router.js';

export default {
    ACTION_getRoom(contex, roomName) {
        const getURL = contex.getters.GET_serverAdress + "/room?name=" + roomName ;
        axios.get(getURL)
          .then(response => { 
            contex.commit('SET_gameRoom', response.data);
            contex.commit('SET_exist', true);
          });
      },
    ACTION_closeRoom(contex, roomName){
        const deleteURL = contex.getters.GET_serverAdress + "/closeRoom?name=" + roomName;
        axios.delete(deleteURL)
        .then(response => {
            console.log(response);
            contex.commit('SET_isInRoom', false);
        })
        contex.dispatch('ACTION_leaveRoom', roomName);
    },
    ACTION_leaveRoom(contex, roomName) {
        contex.commit('SET_isInRoom', false);
        contex.commit('SET_exist', false);
        contex.commit('DEL_activeCharacter', contex.getters.GET_playerName);
        if(contex.getters.GET_userStatus == "player"){
          const deleteURL = contex.getters.GET_serverAdress + "/removeConnectedPlayer?playerName=" + contex.getters.GET_playerName + "&roomName=" + roomName;
          axios.delete(deleteURL)
          .then(response => console.log(response))
        } else if (contex.getters.GET_userStatus == "gamemaster"){
          const deleteURL = contex.getters.GET_serverAdress + "/removeGamemaster?roomName=" +  roomName;
          axios.delete(deleteURL)
          .then(response => console.log(response))
        }
        localStorage.removeItem('vuex');
        router.push("/homeroom");
    },
    ACTION_getPresetItems(contex){
        const getURL = contex.getters.GET_serverAdress + "/presetItems";
        axios.get(getURL)
          .then(response => {
            contex.commit('SET_WeaponPreset', response.data.weaponList.weaponList);
            contex.commit('SET_ArmorPreset', response.data.armorList.armorList);
            contex.commit('SET_StatusPreset', response.data.statusList.statusList);
            
          });
      
    }
};