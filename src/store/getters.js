export default {
    GET_serverAdress(state){
        return state.serverAdress;
    },
    GET_serverWSAdress(state){
        return state.serverWSAdress;
    },
    GET_gameroom(state) {
        return state.gameroom;
    }, 
    GET_roomName(state) {
        return state.gameroom.name;
    },
    GET_playerName(state) {
        return state.playerName;
    },
    GET_isInRoom(state){
        return state.isInRoom;
    },
    GET_exist(state){
        return state.exist;
    },
    GET_userStatus(state){
        return state.userStatus;
    },
    GET_gameroomList(state){
        return state.gameroomsList;
    }, 
    GET_players(state){
        return state.gameroom.players;
    },
    GET_enemies(state){
        return state.gameroom.enemies;
    },
    GET_emptyPlayer(state){
        return state.emptyPlayer;
    },
    GET_battlelog(state){
        return state.gameroom.battleLog;
    },
    GET_battlelogUpdate(state){
        return state.battlelogUpdate;
    },
    GET_combatMessage(state){
        return state.combatMessage;
    },
    // ------------------------ INNER SETTINGS --------------------------------------------------
    GET_weapons(state){
        return state.weapons;
    },
    GET_armors(state){
        return state.armors;
    },
    GET_status(state){
        return state.statuses;
    },

    // ------------------------ CRUD CHARACTER --------------------------------------------------
    GET_charToUpdate(state){
        return state.characterUpdate;
    },
    GET_charToDelete(state){
        return state.characterDelete;
    },
};