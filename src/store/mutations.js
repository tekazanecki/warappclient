export default {
    // ---------------------- ROOM ATRIBIUTES --------------------------------------
    SET_isInRoom(state, boolVal){
        state.isInRoom = boolVal;
    },
    SET_gameRoom(state, room){
        state.gameroom = room;
    },
    SET_gameroomName(state, gameroomName){
        state.gameroom.name = gameroomName;
    },
    SET_playerName(state, name) {
        state.playerName = name;
        state.userStatus = "player";
    },
    SET_gamemaster(state, gamemaster){
        state.gameroom.gamemaster = gamemaster;
        state.playerName = gamemaster;
        state.userStatus = "gamemaster";
    },
    SET_roomList(state, lists) {
        state.gameroomsList = lists;
    },
    SET_exist(state, boolVal){
        state.exist = boolVal;
    },
    // ----------------------- PRESET ITEMS -------------------------------------------
    SET_WeaponPreset(state, weapons){
        state.weapons = weapons;
    },
    SET_ArmorPreset(state, armors){
        state.armors = armors;
    },
    SET_StatusPreset(state, statuses){
        state.statuses = statuses;
    },

    // ----------------------- CHARACTER MUTATIONS ------------------------------ 
    SET_gamemasterAsOwner(state){
        state.emptyPlayer.user = "gamemaster";
    },
    SET_userAsOwner(state, userName){
        state.emptyPlayer.user = userName;
    },
    SET_emptyCharWounds(state, wounds){
        state.emptyPlayer.wounds = wounds;
    },
    SET_charWeapon(state, weapon){
        state.characterUpdate.weapon = weapon;
    },
    SET_charArmor(state, armor){
        state.characterUpdate.armor = armor;
    },
    SET_charStatus(state, status){
        state.characterUpdate.status = status;
    },

    // ---------------------- CRUD BATTLELOG ------------------------------------------------
    SET_battleLogUpdate(state, update){ //single message
        state.battlelogUpdate = update;
    },
    ADD_toBattlelog(state, newLog){
        state.gameroom.battleLog = state.gameroom.battleLog.concat(newLog);
    },

    // ----------------------------- COMBAT  -----------------------------------------------
    SET_combatMessageUpdate(state, combatMsg){ //single message
        state.combatMessage = combatMsg;
    },
    SET_playerAttributes(state, payload){
        state.gameroom.players[payload.index].status = payload.status;
        state.gameroom.players[payload.index].currentHealth = payload.currentHealth;
        state.gameroom.players[payload.index].criticalWounds = state.gameroom.players[payload.index].criticalWounds.concat(payload.critics);
    },
    SET_enemyAttributes(state, payload){
        state.gameroom.enemies[payload.index].status = payload.status;
        state.gameroom.enemies[payload.index].currentHealth = payload.currentHealth;
        state.gameroom.enemies[payload.index].criticalWounds = state.gameroom.enemies[payload.index].criticalWounds.concat(payload.critics);
    },

    // ------------------------ CRUD CHARACTER --------------------------------------------------
    ADD_player(state, newPlayer){
        state.gameroom.players.push(JSON.parse(JSON.stringify(newPlayer)));
    },
    ADD_Enemy(state, newEnemy){
        state.gameroom.enemies.push(JSON.parse(JSON.stringify(newEnemy)));     
    },
    DEL_activeCharacter(state, name){
        var index = state.gameroom.connectedPlayers.findIndex(player => player == name);
        console.log("active character deleted: " +index + " name: " + state.gameroom.connectedPlayers[index]);
        state.gameroom.connectedPlayers.splice(index,1);
    },
    SET_specificPlayer(state, payload){
        state.gameroom.players[payload.index] = payload.character;
    },
    SET_specificEnemy(state, payload){
        state.gameroom.enemies[payload.index] = payload.character;
    },
    DEL_specificPlayer(state, index){
        state.gameroom.players.splice(index, 1);
    },
    DEL_specificEnemy(state, index){
        state.gameroom.enemies.splice(index, 1);
    },
    SET_charToRemove(state, charName){
        state.characterDelete = charName;
    },
    SET_charToUpdate(state, character){
        state.characterUpdate = character;
    },
};