export default {
    // ------ USER STATS --------
    playerName: "",
    userStatus: "",
    // ------ ROOM STATS --------
    isInRoom: false,
    exist: false,
    gameroom: null,
    // ------ APLICATION INFO ------
    gameroomsList: null,
    gmNewCharNumber: 1,
    serverAdress: "http://localhost:8080",//"https://whispering-tundra-19559.herokuapp.com",//
    serverWSAdress: "ws://localhost:8080",//"wss://whispering-tundra-19559.herokuapp.com",//
    // ------ OBJECT TO SEND -------           
    battlelogUpdate: '',
    characterUpdate: '',
    characterDelete: '',
    combatMessage: '',
    // ------- EMPTY OBJECTS -------
    emptyPlayer: {
        userID: "Empty01",
        user: "none",
        charName: "characterName",
        charAttr: [20, 20, 20, 20, 20, 20, 20, 20, 20, 20],
        charSkill: [0, 0],
        wounds: 8,
        currentHealth: 8,
        weapon: "unarmed",
        armor: "none",
        criticalWounds: [
        ],
        status: "normal",
        editable: true
    },
    // ------- OPTIONS -----------
    weapons:  [],
    armors: [],
    statuses: []
}