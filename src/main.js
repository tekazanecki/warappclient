import { createApp } from 'vue';
import Router from './router.js';
import Store from './store/index.js';
import PrimeVue from 'primevue/config';
import axios from 'axios';
import VueAxios from 'vue-axios';

import App from './App.vue';
import GenericCard from './components/genericUI/GenericCard.vue';
import GenericButton from './components/genericUI/GenericButton.vue';
import TheHeader from './components/layout/TheHeader.vue'
import CriticalChip from './components/prime/CriticalChip.vue';

import Dialog from 'primevue/dialog';
import Button from 'primevue/button';
import Message from 'primevue/message';
import InputText from 'primevue/inputtext';
import InputNumber from 'primevue/inputnumber';
import Menubar from 'primevue/menubar';
import Checkbox from 'primevue/checkbox';
import Dropdown from 'primevue/dropdown';
import DataTable from 'primevue/datatable';
import Column from 'primevue/column';


import 'primevue/resources/themes/saga-blue/theme.css'
import 'primevue/resources/primevue.min.css'
import 'primeicons/primeicons.css'
import 'primeflex/primeflex.css';



const app = createApp(App);

app.use(Router);
app.use(Store);
app.use(PrimeVue);
app.use(VueAxios, axios)

app.component('Dialog', Dialog);
app.component('generic-card', GenericCard);
app.component('generic-button', GenericButton);
app.component('the-header', TheHeader);
app.component('CriticalChip', CriticalChip);

app.component('InputText', InputText);
app.component('InputNumber', InputNumber);
app.component('Menubar', Menubar);
app.component('Message', Message);
app.component('Button', Button);
app.component('Checkbox', Checkbox);
app.component('Dropdown', Dropdown);
app.component('DataTable', DataTable);
app.component('Column', Column);



app.mount('#app');

