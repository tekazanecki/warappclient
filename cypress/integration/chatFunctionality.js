describe('chat_functionality', function() {

    it('check if chat display text and generate simple test results', function() {
   
        cy.viewport(2400, 1211)
 
        cy.visit('http://localhost:8081/homeroom')
     
        cy.get('.card > form > .p-formgroup-block > .p-field > #playername').click()
     
        cy.get('.card > form > .p-formgroup-block > .p-field > #playername').type('TomaszTestChat')
     
        cy.get('.p-mr-5 > .card > form > .p-formgroup-block > .p-field > #roomname').click()
     
        cy.get('.p-mr-5 > .card > form > .p-formgroup-block > .p-field > #roomname').type('Test Room')
     
        cy.get('.p-mr-5 > .card > form > .p-formgroup-block > #buttonsJoin > .outline').click()
     
        cy.get('.p-mr-5 > .card > form > .p-formgroup-block > #buttonsJoin > button:nth-child(2)').click()
     
        cy.get('#app > section > #roomFound > .gameroomClass > button:nth-child(3)').click()
     
        cy.get('.card > section > div > .p-input-icon-right > #roomNameInput').click()
     
        cy.get('.card > section > div > .p-input-icon-right > #roomNameInput').type('To jest test')
     
        cy.get('.card > section > div > .p-input-icon-right > .pi').click()
     
        cy.get('.card > section > div > .p-input-icon-right > #roomNameInput').click()
     
        cy.get('.card > section > div > .p-input-icon-right > #roomNameInput').type('#roll Clesvel WS +20')
     
        cy.get('.card > section > div > .p-input-icon-right > .pi').click()
     
        cy.get('.card > section > div > .p-input-icon-right > #roomNameInput').click()
     
        cy.get('.card > section > div > .p-input-icon-right > #roomNameInput').type('#roll (Goblin Grunt) WP +20')
     
        cy.get('.card > section > div > .p-input-icon-right > .pi').click()
     
        cy.get('#app > section > #roomFound > .gameroomClass > button:nth-child(1)').click()
     
     })
    
   
   })