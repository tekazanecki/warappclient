describe('character_CRUD', function() {

 it('create, edit and remove characters in player and enemy sections', function() {

    cy.viewport(2400, 1211)
 
    cy.visit('http://localhost:8081/homeroom')
 
    cy.get('.card > form > .p-formgroup-block > .p-field > #gamemaster').click()
 
    cy.get('.card > form > .p-formgroup-block > .p-field > #gamemaster').type('Tomasz')
 
    cy.get('.p-ml-5 > .card > form > .p-formgroup-block > .p-field > #roomname').click()
 
    cy.get('.p-ml-5 > .card > form > .p-formgroup-block > .p-field > #roomname').type('Tower of Night01')
 
    cy.get('.p-ml-5 > .card > form > .p-formgroup-block > #buttonsJoin > .outline').click()
 
    cy.get('form > .p-formgroup-block > #buttonsJoin > button > .pi').click()
 
    cy.get('#app > section > #roomFound > .gameroomClass > button:nth-child(4)').click()

    cy.wait(500)
 
    cy.get('#char > .card:nth-child(1) > .p-d-flex > .p-button > .pi').click()
 
    cy.get('#app > section > #roomFound > #gameroom > #char').click()

    cy.get('.card > div > .p-grid > .p-col-8 > .p-inputtext').clear()
 
    cy.get('.card > div > .p-grid > .p-col-8 > .p-inputtext').type('Player01')
 
    cy.get('.card > div > .p-mt-2 > div > button:nth-child(1)').click()
 
    cy.get('div > .card > div > .p-mt-2 > button:nth-child(2)').click()
 
    cy.get('.card > div > .p-grid > .p-col-8 > .p-inputtext').click()
 
    cy.get('.card > div > .p-grid > .p-col-8 > .p-inputtext').clear()

    cy.get('.card > div > .p-grid > .p-col-8 > .p-inputtext').type('Player01Change')
 
    cy.get('.card > div > .p-mt-2 > div > button:nth-child(1)').click()
 
    cy.get('#char > .card:nth-child(2) > .p-d-flex > .p-button > .pi').click()
 
    cy.get('section > #roomFound > #gameroom > #char > .card:nth-child(2)').click()
 
    cy.get('.card > div > .p-grid > .p-col-8 > .p-inputtext').clear()

    cy.get('.card > div > .p-grid > .p-col-8 > .p-inputtext').type('Enemy01')

 
    cy.get('.card > div > .p-mt-2 > div > button:nth-child(1)').click()
 
    cy.get('.card:nth-child(2) > div > .card > div > .p-mt-2 > button:nth-child(2)').click()
 
    cy.get('.card > div > .p-grid > .p-col-8 > .p-inputtext').click()

    cy.get('.card > div > .p-grid > .p-col-8 > .p-inputtext').clear()
 
    cy.get('.card > div > .p-grid > .p-col-8 > .p-inputtext').type('Enemy01Change')
 
    cy.get('.card > div > .p-mt-2 > div > button:nth-child(1)').click()
 
    cy.get('.card:nth-child(1) > div > .card > div > .p-mt-2 > button:nth-child(3)').click()
 
    cy.get('div > .card > div > .p-mt-2 > button:nth-child(3)').click()
 
    cy.get('#app > section > #roomFound > .gameroomClass > button:nth-child(2)').click()
 
 })

 })
