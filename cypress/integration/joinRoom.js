describe('join_room', function() {

    it('join room and leave it as player and as gamemaster', function() {
   
       cy.viewport(2400, 1211)
    
       cy.visit('http://localhost:8081/homeroom')
    
       cy.get('.card > form > .p-formgroup-block > .p-field > #playername').click()
    
       cy.get('.card > form > .p-formgroup-block > .p-field > #playername').type('TomaszTestJoin')
    
       cy.get('.p-mr-5 > .card > form > .p-formgroup-block > .p-field > #roomname').type('Test Room')
    
       cy.get('.p-mr-5 > .card > form > .p-formgroup-block > #buttonsJoin > .outline').click()
    
       cy.get('.p-mr-5 > .card > form > .p-formgroup-block > #buttonsJoin > button:nth-child(2)').click()
    
       cy.get('#app > section > #roomFound > .gameroomClass > button:nth-child(3)').click()
    
       cy.get('#app > section > #roomFound > .gameroomClass > button:nth-child(1)').click()
    
       cy.get('.card > form > .p-formgroup-block > .p-field > #playername').click()
    
       cy.get('.card > form > .p-formgroup-block > .p-field > #playername').type('TomaszTest01')
    
       cy.get('.p-mr-5 > .card > form > .p-formgroup-block > .p-field > #roomname').type('Orphan Room')
    
       cy.get('.p-mr-5 > .card > form > .p-formgroup-block > .p-field-checkbox').click()
    
       cy.get('form > .p-formgroup-block > .p-field-checkbox > .p-checkbox > .p-checkbox-box').click()
    
       cy.get('.p-mr-5 > .card > form > .p-formgroup-block > #buttonsJoin > .outline').click()
    
       cy.get('.p-mr-5 > .card > form > .p-formgroup-block > #buttonsJoin > button:nth-child(2)').click()
    
       cy.get('#app > section > #roomFound > .gameroomClass > button:nth-child(4)').click()
    
       cy.get('#app > section > #roomFound > .gameroomClass > button:nth-child(1)').click()
    
    })
   
   })