describe('create_room', function() {

   it('create and cloase room', function() {
  
      cy.viewport(2400, 1211)
   
      cy.visit('http://localhost:8081/homeroom')
   
      cy.get('.card > form > .p-formgroup-block > .p-field > #gamemaster').click()
   
      cy.get('.card > form > .p-formgroup-block > .p-field > #gamemaster').type('Tomasz')
   
      cy.get('.p-ml-5 > .card > form > .p-formgroup-block > .p-field > #roomname').type('Tower of Night02')
   
      cy.get('.p-ml-5 > .card > form > .p-formgroup-block > #buttonsJoin > .outline').click()
   
      cy.get('.p-ml-5 > .card > form > .p-formgroup-block > #buttonsJoin > button:nth-child(2)').click()
   
      cy.get('#app > section > #roomFound > .gameroomClass > button:nth-child(4)').click()
   
      cy.get('#app > section > #roomFound > .gameroomClass > button:nth-child(2)').click()
   
   })
  
  })