describe('attacker_check', function() {

    it('make few attacks between characters and enemies', function() {
   
       cy.viewport(2400, 1211)
    
       cy.visit('http://localhost:8081/homeroom')
    
       cy.get('.card > form > .p-formgroup-block > .p-field > #playername').click()
    
       cy.get('.card > form > .p-formgroup-block > .p-field > #playername').type('Tomasz')
    
       cy.get('.p-mr-5 > .card > form > .p-formgroup-block > .p-field > #roomname').click()
    
       cy.get('.p-mr-5 > .card > form > .p-formgroup-block > .p-field > #roomname').type('Orphan Room')
    
       cy.get('form > .p-formgroup-block > .p-field-checkbox > .p-checkbox > .p-checkbox-box').click()
    
       cy.get('.p-mr-5 > .card > form > .p-formgroup-block > #buttonsJoin > .outline').click()
    
       cy.get('form > .p-formgroup-block > #buttonsJoin > button > .pi').click()
    
       cy.get('#app > section > #roomFound > .gameroomClass > button:nth-child(4)').click()
    
       cy.get('.card:nth-child(1) > div > .card:nth-child(1) > div > .p-mt-2 > button:nth-child(1)').click()
    
       cy.get('.p-mt-2 > .p-d-flex > div > .p-dropdown > .p-dropdown-label').click()
    
       cy.get('.p-dropdown > .p-dropdown-panel > .p-dropdown-items-wrapper > .p-dropdown-items > .p-dropdown-item').click()
    
       cy.get('.card:nth-child(1) > div > .card:nth-child(1) > div > .p-mt-2 > button:nth-child(1)').click()
    
       cy.get('.card:nth-child(2) > div > .card > div > .p-mt-2 > button:nth-child(1)').click()
    
       cy.get('.p-mt-2 > .p-d-flex > div > .p-dropdown > .p-dropdown-label').click()
    
       cy.get('.p-dropdown > .p-dropdown-panel > .p-dropdown-items-wrapper > .p-dropdown-items > .p-dropdown-item:nth-child(1)').click()
    
       cy.get('.card:nth-child(2) > div > .card > div > .p-mt-2 > button:nth-child(1)').click()
    
       cy.get('div > .card:nth-child(2) > div > .p-mt-2 > button:nth-child(1)').click()
    
       cy.get('.p-mt-2 > .p-d-flex > div > .p-dropdown > .p-dropdown-label').click()
    
       cy.get('.p-dropdown > .p-dropdown-panel > .p-dropdown-items-wrapper > .p-dropdown-items > .p-dropdown-item').click()
    
       cy.get('div > .card:nth-child(2) > div > .p-mt-2 > button:nth-child(1)').click()
    
       cy.get('.card:nth-child(2) > div > .card > div > .p-mt-2 > button:nth-child(1)').click()
    
       cy.get('.p-mt-2 > .p-d-flex > div > .p-dropdown > .p-dropdown-label').click()
    
       cy.get('.p-dropdown > .p-dropdown-panel > .p-dropdown-items-wrapper > .p-dropdown-items > .p-dropdown-item:nth-child(2)').click()
    
       cy.get('.card:nth-child(2) > div > .card > div > .p-mt-2 > button:nth-child(1)').click()
    
       cy.get('div > .card:nth-child(3) > div > .p-mt-2 > button:nth-child(1)').click()
    
       cy.get('.p-mt-2 > .p-d-flex > div > .p-dropdown > .p-dropdown-label').click()
    
       cy.get('.p-dropdown > .p-dropdown-panel > .p-dropdown-items-wrapper > .p-dropdown-items > .p-dropdown-item').click()
    
       cy.get('div > .card:nth-child(3) > div > .p-mt-2 > button:nth-child(1)').click()
    
       cy.get('.card:nth-child(2) > div > .card > div > .p-mt-2 > button:nth-child(1)').click()
    
       cy.get('.p-mt-2 > .p-d-flex > div > .p-dropdown > .p-dropdown-label').click()
    
       cy.get('.p-dropdown > .p-dropdown-panel > .p-dropdown-items-wrapper > .p-dropdown-items > .p-dropdown-item:nth-child(3)').click()
    
       cy.get('.card:nth-child(2) > div > .card > div > .p-mt-2 > button:nth-child(1)').click()

       cy.get('#app > section > #roomFound > .gameroomClass > button:nth-child(2)').click()
    
    })
   
   })
